var conf = require('../../nightwatch.conf.js');

module.exports = {
  'Open Website Vente privée': function (browser) {
    browser
      .resizeWindow(1280, 800)
      .url('https://secure.fr.vente-privee.com/authentication/Portal/FR')
      .waitForElementVisible('body',1000)
      .assert.title('Veepee : grandes marques à prix discount, ventes événementielles chaque jour.')

      browser.isVisible('.cookiesBtn', function(result) {
        if(result.status != -1) { //Element exists, close cookie
          browser.click('.cookiesBtn')
        }
      });

      browser.isVisible('#txtEmail', function(result) {
        console.log('txtEmail exist');
      });

      browser.isVisible('#txtPassword', function(result) {
        console.log('txtPassword exist');
      });

      browser.isVisible('#btSubmit', function(result) {
        console.log('btSumbit exist');
      });

      browser.isVisible('#signUpLink', function(result) {
        if(result.status != -1) { 
          browser
            .click('#signUpLink')
            .waitForElementVisible('body',20000)
            .assert.title('Devenez membre vente-privée : chaque jour les plus grandes marques à prix discount')
            .assert.urlEquals('https://secure.fr.vente-privee.com/registration/registration?CountryCodeUser=FR&accessButtonText=Inscrivez-vous%20maintenant%20!')

            browser.isVisible('#mainForm > fieldset > div:nth-child(2) > div:nth-child(2)', function(result) {
              console.log("checkbox homme exist");     
            });

            browser.isVisible('#mainForm > fieldset > div:nth-child(2) > div:nth-child(3)', function(result) {
              console.log("checkbox femme exist");   
            });

            browser.isVisible('#txtLastName', function(result) {
              console.log("fild name exist");   
            });

            browser.isVisible('#txtFirstName', function(result) {
              console.log("checkbox firstname exist");   
            });

            browser.isVisible('#txtMail', function(result) {
              console.log("checkbox txtmail exist");   
            });

            browser.isVisible('#txtPassword', function(result) {
              console.log("checkbox password exist");   
            });

            browser.isVisible('#ckbInvitMailPartner', function(result) {
              console.log("radio invitmailpartner exist");   
            });

            browser.isVisible('#registerBt', function(result) {
              console.log("button register exist");
              if(result.status != -1) { 
                browser
                .click('#registerBt')
                .waitForElementVisible('#CivilityValidate',5000)
                .waitForElementVisible('#txtFirstNameValidate',1000)
                .waitForElementVisible('#txtLastNameValidate',1000)
                .waitForElementVisible('#txtMailValidate',1000)
                .waitForElementVisible('#txtPasswordValidate',1000)
              }   
            });

            browser
              .click('#mainForm > fieldset > div:nth-child(2) > div:nth-child(2)')
              .setValue('#txtLastName', '2cte2cebiiephima')
              .setValue('#txtFirstName', 'yolsqqso')
              .setValue('#txtMail', 'yoloeeee@yopmail.com')
              .setValue('#txtPassword', 'yo@lqsdfqdsfqdsfqsdo')
              .click('#ckbInvitMailPartner')
              .getValue('#txtLastName', function(result) {
                console.log(result)
              })
              .click('#registerBt')
              .waitForElementVisible('body',20000)


              browser.element('css selector', '#menuBtn > div.css-c2uxn6.eds3m1a5 > svg', function(result) {
                if(result.status != -1) { //Element exists, do something
                  browser.waitForElementVisible('#root > div > div.wrapper > header > div > div > div.css-mreiyi.eo8itkx2 > div.css-0.ef7f8yv0 > div.css-t6pi78.ef7f8yv1 > ul:nth-child(5) > li > a',1000)
                }
              });
        }
      });

      browser.end();
    }
  };
